package com.valiit.companiesapi.repository;

import com.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Company getCompany(int id) {
        List<Company> companies = jdbcTemplate.query("SELECT * FROM company WHERE id = ?", new Object[]{id},
                (rs, rowNum) -> new Company(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("logo")
                ));
        return !companies.isEmpty() ? companies.get(0) : null;
    }

    public List<Company> getCompanies() {
        List<Company> companies = jdbcTemplate.query("SELECT * FROM company",
                (rs, rowNum) -> new Company(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("logo")));
        return companies;
    }

    public void addCompany(Company company) {
        jdbcTemplate.update("INSERT INTO company (name, logo) VALUES (?, ?)",
                company.getName(), company.getLogo());
    }

    public void changeCompany(Company company) {
        jdbcTemplate.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
                company.getName(), company.getLogo(), company.getId());
    }

    public void deleteCompany(int id) {
        jdbcTemplate.update("DELETE FROM company WHERE id = ?", id);
    }

}
