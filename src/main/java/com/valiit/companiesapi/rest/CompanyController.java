package com.valiit.companiesapi.rest;

import com.valiit.companiesapi.model.Company;
import com.valiit.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping
    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/company/{id}")
    public Company getCompany(@PathVariable int id){
        return companyRepository.getCompany(id);
    }

    @PostMapping("/company")
    public void addCompany(@RequestBody Company company) {
        companyRepository.addCompany(company);
    }

    @PutMapping("/company")
    public void changeCompany(@RequestBody Company company) {
        companyRepository.changeCompany(company);
    }

    @DeleteMapping("/company/{id}")
    public void deleteCompany(@PathVariable int id) {
        companyRepository.deleteCompany(id);
    }
}
